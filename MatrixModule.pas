unit MatrixModule;

interface
const
    Size = 9;
    BeginOfCut = -21;
    EndOfCut = 23;

type TypeArray = array [1..Size, 1..Size] of Real;
    
  procedure CreateMatrix(var inputArray: TypeArray);
  procedure PrintMatrix(var inputArray: TypeArray);
  procedure FindPositiveElements(var inputArray: TypeArray);
  procedure ChangeNegativeToZero(var inputArray: TypeArray);
  
 implementation

procedure CreateMatrix(var inputArray: TypeArray);
   var i,j : Integer;
 
    begin
      for i := 1 to Size do 
        begin
          for j := 1 to Size do
            begin
              inputArray[i,j] := Random(EndOfCut - BeginOfCut + 1) + BeginOfCut;
            end
    	  end
    end;

    
procedure PrintMatrix(inputArray: TypeArray);
 var i,j : Integer;
 
    begin
      for i := 1 to Size do 
        begin
          for j := 1 to Size do
            begin
              Write(inputArray[i,j]);
            end;
            WriteLn;
    	  end
    end;
    
procedure FindPositiveElements(var inputArray: TypeArray);
    var
        i, j, Count : Integer;
    begin
    for i:= 1 to Size do
            begin
                  for j := 1 to Size do
                      begin
                          if (inputArray[i,j] > 0) AND (i + j = Size + 1) then
                              Count := Count + 1;
                      end;
            end;
        WriteLn('Result: ', Count);
    end;
    
 procedure ChangeNegativeToZero(var inputArray: TypeArray);
     var i, j: Integer;

    begin
         for i:=1 to Size do
             for j:=1 to Size do
                 begin
                  if ((i>j) and (inputArray[i,j]<0)) then
                  inputArray[i,j]:=0;
                 end;
    end;
end.