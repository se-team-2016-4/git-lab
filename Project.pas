program Project;

uses
    MatrixModule;

const
    Size = 9;
    BeginOfCut = -21;
    EndOfCut = 23;

type
    TypeArray = array [1..Size, 1..Size] of Real;

begin
     CreateMatrix(inputArray);
     PrintMatrix(inputArray);
     WriteLn;
     FindPositiveElements(inputArray);
     WriteLn;
     ChangeNegativeToZero(inputArray);
     PrintMatrix(inputArray);
end.
